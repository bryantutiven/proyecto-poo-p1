/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Clientes {
    
    protected int codigo;
    protected String nombre;
    protected String direccion;
    protected int telefono;
    protected String tipo;

    public Clientes() {
        this.codigo = 0;
        this.nombre = "";
        this.direccion = "";
        this.telefono = 0;
        this.tipo = "";
    }

    public Clientes(int codigo, String nombre, String direccion, int telefono, String tipo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.tipo = tipo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public static ArrayList<Clientes> leerArchivoC(){
        ArrayList<Clientes> listaClientes = new ArrayList<Clientes>();


        String csvFile = "clientes.csv";
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                Clientes usuario = new Clientes(Integer.parseInt(datos[0]),datos[1],datos[2],Integer.parseInt(datos[3]),datos[4]);
                listaClientes.add(usuario);
            }
                      
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return listaClientes;   
    }
    
    public static void mostrarInformacion(ArrayList<Clientes> listaClientes){
        for(Clientes c: listaClientes ){
            System.out.println("Código \tNombre \tDirección \tTeléfono \tTipo de Cliente");
            System.out.println(c.getCodigo()+"\t"+c.getNombre()+"\t"+c.getDireccion()+"\t"+c.getTelefono()+"\t"+c.getTipo());            
        }
    }
    
    public static boolean validarCliente(ArrayList<Clientes> listaClientes, String codigoCliente){
        boolean validez = false;
        for(Clientes c: listaClientes ){
            if((c.getCodigo())==Integer.parseInt(codigoCliente)){
                validez = true;
            }            
        }
        return validez;
    }
    
    public static void mostrarInformacion(ArrayList<Clientes> listaClientes, String codigoCliente){
        for(Clientes c: listaClientes ){
            if((c.getCodigo())==Integer.parseInt(codigoCliente)){
                System.out.println(c.getNombre()+"\t"+c.getDireccion()+"\t"+c.getTelefono()+"\t"+c.getTipo());
            }            
        }
    }
}
