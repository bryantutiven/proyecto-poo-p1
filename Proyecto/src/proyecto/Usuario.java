package proyecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Usuario {
    protected String id_usuario;
    protected int contraseña;
    protected String nombre;
    protected String nivel;
    protected boolean tecnico;
    protected boolean admin;

    public Usuario() {
        this.id_usuario = "";
        this.contraseña = 0;
        this.nombre = "";
        this.nivel = "";
        this.tecnico = false;
        this.admin = false;
        
    }
    
    public Usuario(String id_usuario, int contraseña, String nombre, String nivel) {
        this.id_usuario = id_usuario;
        this.contraseña = contraseña;
        this.nombre = nombre;
        this.nivel = nivel;   
    }
    
    public boolean usuarioTecnico(){
        if(nivel.equals("tecnico")){
            this.tecnico = true;
        }
        return this.tecnico;
    }
    
    public String getId() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getContraseña() {
        return contraseña;
    }

    public void setContraseña(int contraseña) {
        this.contraseña = contraseña;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public static ArrayList<Usuario> leerArchivoU(){
        ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();


        String csvFile = "usuarios.csv";
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                Usuario usuario = new Usuario(datos[0],Integer.parseInt(datos[1]),datos[2],datos[3]);
                listaUsuarios.add(usuario);
            }
                      
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return listaUsuarios;   
    }
    
}

