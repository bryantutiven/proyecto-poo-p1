/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import static proyecto.Clientes.leerArchivoC;
import static proyecto.IPs.leerArchivoIPs;
import static proyecto.Paises.leerArchivoP;
import static proyecto.Proveedores.leerArchivoPr;
import static proyecto.Tarifas.leerArchivoT;
import static proyecto.Usuario.leerArchivoU;


//objeto Administrador
public class Administrador extends Usuario{
    ArrayList<Usuario> listaUsuarios = leerArchivoU();
    ArrayList<Clientes> listaClientes = leerArchivoC();
    ArrayList<IPs> listaIPs = leerArchivoIPs();
    ArrayList<Paises> listaPaises = leerArchivoP();
    ArrayList<Proveedores> listaProveedores = leerArchivoPr();
    ArrayList<Tarifas> listaTarifas = leerArchivoT();       
    
    //constructor del objeto administrador
    public Administrador(String id_usuario, int contraseña, String nombre, String nivel){
        super(id_usuario, contraseña,nombre,nivel);
        this.id_usuario = id_usuario;
        this.contraseña = contraseña;
        this.nombre = nombre;
        this.nivel = nivel;
        
        
    }
    
    
    public void menuAdmin(){
        System.out.println("1.Detalle Factura Cliente"
                + "Reporte Llamadas por Clientes por Mes"
                + "Reporte Llamadas por Proveedor por Mes");
        
    }
    
    //metodos de opcion 1
    //(ingresa el nombre de un archivo y devuelve verdadero o falso)
    public boolean existenciaFacturas(String nombreArchivo){
        
        File archivo = new File(nombreArchivo);
        
        boolean existencia = archivo.exists();
        return existencia;
        }
        
    //crear un arreglo de clientes segun los datos de factura
    public static ArrayList<RegistroClientes>leerArchivo(){
        ArrayList<RegistroClientes>ListaRegistro = new ArrayList();
        
        
        String csvFile = "llamadas_facturadas.csv";
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                RegistroClientes u = new RegistroClientes(datos[0],datos[1],datos[2],datos[3],datos[4],datos[5],Integer.parseInt(datos[6]),Integer.parseInt(datos[7]),Integer.parseInt(datos[8]),Integer.parseInt(datos[9]),datos[10],datos[11]);
                ListaRegistro.add(u);
            }

              
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
            }
                
        }
    }
        
        return ListaRegistro;
    }
    //Sumario de clientes por mes
   
    public void clientesMes(){
        
    }
    
}
