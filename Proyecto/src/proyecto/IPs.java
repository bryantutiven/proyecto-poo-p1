
package proyecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
public class IPs {
    protected String ip;
    protected String tipo;
    protected int codigo;

    public IPs() {
        this.ip = "";
        this.tipo = "";
        this.codigo = 0;
    }

    public IPs(String ip, String tipo, int codigo) {
        this.ip = ip;
        this.tipo = tipo;
        this.codigo = codigo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public static ArrayList<IPs> leerArchivoIPs(){
        ArrayList<IPs> listaIPs = new ArrayList<IPs>();


        String csvFile = "ips.csv";
        BufferedReader br = null;
        String line = " ";
        //Se define separador ","
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                IPs usuario = new IPs(datos[0],datos[1],Integer.parseInt(datos[2]));
                listaIPs.add(usuario);
            }
                      
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return listaIPs;   
    }
    public void InformacionIPs(){//Metodo capara la obtencion de informacion o editar datos.
        ArrayList<IPs> listaIPs = leerArchivoIPs();
        Scanner input = new Scanner(System.in);
        String opcion;
        String acceso1 = "true", acceso2 = "true";
        int c=0;
        while(acceso1.equals("true")){
            System.out.println("Elija una opcion:\n"
                    + "\t1. Mostrar listado de IPs.\n"
                    + "\t2. Agregar Ips.\n"
                    + "\t3. Editar algun campo de cierta IP.\n"
                    + "\t4. Volver al menu.\n");
            opcion = input.next();
            if(opcion.equals("1")){
                System.out.println("Ha elegido la opcion 1 de Mostrar listado de IPs.");
                System.out.println("Ip \t\t Tipo de IP \t Codigo Cliente/Proveedor");
                for(IPs i : listaIPs){
                    System.out.println(i.getIp()+"\t\t"+i.getTipo()+"\t\t"+i.getCodigo());
                }
            }
            else if(opcion.equals("2")){
                System.out.println("Ha elegido la opcion 2 de Agregar Ips.");
                while(acceso2.equals("true")){
                System.out.println("Ingrese Ip:");
                String ip = input.next();
                String numerosP="0123456789.";
                for(IPs p: listaIPs){
                    String validar1=" ", validar2=" ";
                    if(!ip.equals(p.getIp())){
                        for(int i=0; i<ip.length();i++){
                            if (numerosP.indexOf(ip.charAt(i),0)!=-1){
                                c++;
                                } 
                            if(c==ip.length()){
                                System.out.println("Es correcto el ingreso.");
                                validar1 = "true";
                            }
                        }
                    }
                else{
                    System.out.println("Error de Ip contiene caracteres no validos. use solo numeros o \".\"");
                }
                    while(validar1.equals("true")){
                        System.out.println("Ingrese Tipo de IP \"C\" si es Cliente o \"P\" si es Proveedor");
                        String tip = input.next();
                        if(tip.toUpperCase().length()==1 && (tip.equalsIgnoreCase("c")|| tip.equalsIgnoreCase("p"))){
                            System.out.println("Es correcto.");
                            validar2 = "true";
                            }
                        else{
                            System.out.println("Error de Tipo ingrese \"C\" o \"P\"");
                        }
                        while(validar2.equals("true")){
                            System.out.println("Ingrese codigo de cliente o proveedor:");
                            String codigo = input.next();
                            if(codigo.matches("[0-9]*" )==true && codigo.length()==1){
                                IPs usuario = new IPs(ip,tip,Integer.parseInt(codigo));
                                listaIPs.add(usuario);
                                validar2 = "false";
                                validar1 = "false";
                                acceso2 = "false";
                                System.out.println("Registro Exitoso!!!!");
                                System.out.println("Ip \t\t Tipo de IP \t Codigo Cliente/Proveedor");
                                for(IPs d: listaIPs){
                                    System.out.println(d.getIp()+"\t\t"+d.getTipo()+"\t\t"+d.getCodigo());
                                }
                            }
                            else{
                                System.out.println("Error de Codigo Vuelva a intentarlo.");
                            }
                        }
                    }
                    break;
                }   
                }
            }
            else if(opcion.equals("3")){
                String acceso3 = "true";
                System.out.println(" Ha eleguido la opcion 3 Editar algun campo de cierta IP");
                while(acceso3.equals("true")){
                    String validacion1="true", validacion2="true";
                    int contador =0;
                    String opcion3;
                    System.out.println("\n Elija entre estas opciones:"
                            + "\n\t 1. Editar ip."
                            + "\n\t 2. Editar tipo."
                            + "\n\t 3. Editar codigo."
                            + "\n\t 4. Volver al menú.");
                    opcion3 = input.next();
                    if(opcion3.equals("1")){
                        while(validacion1.equals("true")){
                            System.out.println("Ha elegido la opcion de Editar Ip.\n Agregue Ip a cambiar.");
                            String ip = input.next();
                            for(IPs i: listaIPs){
                                contador+=1;
                                if(ip.equals(i.getIp())){
                                    System.out.println("IP localizada. Ingrese nueva IP a cambiar:");
                                    String ipc = input.next();
                                    i.setIp(ipc);
                                    System.out.println("El cambio de IP fue un exito.");
                                    System.out.println("Ip \t\t Tipo de IP \t Codigo Cliente/Proveedor");
                                    for(IPs d: listaIPs){
                                        System.out.println(d.getIp()+"\t\t"+d.getTipo()+"\t\t"+d.getCodigo());
                                    }
                                    validacion1 = "false";
                                }
                                else if(contador == listaIPs.size() && !ip.equals(i.getIp())){
                                System.out.println("La IP que desea cambiar no esta en la base de datos. Vuelva a intentarlo.");
                                }
                        }
                        }
                        
                    }
                    else if(opcion3.equals("2")){
                        while(validacion1.equals("true")){
                            System.out.println("Ha elegido la opcion de Editar Tipo.");
                            System.out.println("Agregue Ip para lozalizar el tipo.");
                            String ip = input.next();
                            for(IPs i: listaIPs){
                                contador+=1;
                                if(ip.equals(i.getIp())){
                                    System.out.println("IP localizada.");  
                                    while(validacion2.equals("true")){
                                        System.out.println("Agregue tipo a cambiar.");
                                        String tipo = input.next();
                                        if(tipo.equalsIgnoreCase("c")|| tipo.equalsIgnoreCase("p")){
                                            i.setTipo(tipo);
                                            System.out.println("Cambio de tipo Exitoso.");
                                            System.out.println("Ip \t\t Tipo de IP \t Codigo Cliente/Proveedor");
                                            for(IPs d: listaIPs){
                                                System.out.println(d.getIp()+"\t\t"+d.getTipo()+"\t\t"+d.getCodigo());
                                            }
                                            validacion1 = "false";
                                            validacion2 = "false";
                                        }
                                        else{
                                            System.out.println("Error de Tipo, recuerde que solo puede ser: \"c\" o \"p\". Vuelva  intentarlo");
                                        }
                                    }
                                }
                                else if(contador == listaIPs.size() && !ip.equals(i.getIp())){
                                System.out.println("La IP ingresada  no esta en la base de datos. Vuelva a intentarlo.");
                                }
                        }
                        }
                    }
                    else if(opcion3.equals("3")){
                        while(validacion1.equals("true")){
                            System.out.println("Ha elegido la opcion de Editar codigo.");
                            System.out.println("Agregue Ip para lozalizar el codigo.");
                            String ip = input.next();
                            for(IPs i: listaIPs){
                                contador+=1;
                                if(ip.equals(i.getIp())){
                                    System.out.println("IP localizada.");  
                                    while(validacion2.equals("true")){
                                        System.out.println("Agregue codigo a cambiar.");
                                        String codigo = input.next();
                                        if(codigo.matches("[0-9]*" )==true && codigo.length()==1){
                                            i.setCodigo(Integer.parseInt(codigo));
                                            System.out.println("Cambio de codigo Exitoso.");
                                            System.out.println("Ip \t\t Tipo de IP \t Codigo Cliente/Proveedor");
                                            for(IPs d: listaIPs){
                                                System.out.println(d.getIp()+"\t\t"+d.getTipo()+"\t\t"+d.getCodigo());
                                            }
                                            validacion1 = "false";
                                            validacion2 = "false";
                                        }
                                        else{
                                            System.out.println("Error de codigo. Vuelva  intentarlo");
                                        }
                                    }
                                }
                                else if(contador == listaIPs.size() && !ip.equals(i.getIp())){
                                System.out.println("La IP ingresada  no esta en la base de datos. Vuelva a intentarlo.");
                                }
                        }
                        }
                    }
                    else{
                        System.out.println("Ha elegido la opcion de salir de este submenu.");
                        acceso3="false";
                    }
                }
            }
            else if(opcion.equals("4")){
                acceso1 = "false";
            }
            else{
                System.out.println("Error de opcion vuelva a intentarlo.");
            }
        }
    }
       
}
