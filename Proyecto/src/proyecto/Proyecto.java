package proyecto;

import java.util.ArrayList;
import java.util.Scanner;
import static proyecto.Usuario.leerArchivoU;
import static proyecto.Clientes.leerArchivoC;
import static proyecto.Clientes.mostrarInformacion;
import static proyecto.Clientes.validarCliente;
import static proyecto.IPs.leerArchivoIPs;
import static proyecto.Paises.leerArchivoP;
import static proyecto.Proveedores.leerArchivoPr;
import static proyecto.Tarifas.leerArchivoT;
public class Proyecto {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String usuario,consulta;
        int contraseña = 0;
        int op = 0;
        boolean acceso = true;
        
        ArrayList<Usuario> listaUsuarios = leerArchivoU();
        ArrayList<Clientes> listaClientes = leerArchivoC();
        ArrayList<IPs> listaIPs = leerArchivoIPs();
        ArrayList<Paises> listaPaises = leerArchivoP();
        ArrayList<Proveedores> listaProveedores = leerArchivoPr();
        ArrayList<Tarifas> listaTarifas = leerArchivoT();

        //INICIO DE SESIÓN
        
        //Validar el ingreso de la cadena de strings de usuario
        while (acceso) {
            System.out.print("Ingrese nombre de usuario: ");
            if (sc.hasNextLine()) {//si el ingreso es un string, se lo leera
                usuario = sc.next();

                for (Usuario p : listaUsuarios) { //Recorro el ArrayList con la información del documento ususarios.csv
                    if (usuario.toLowerCase().equals(p.getId())) { //Verificar que el usuario ingresado se encuetre en el array 

                        //Validar el ingreso de la contraseña
                        while (contraseña == 0) {
                            System.out.print("Ingrese contraseña: ");
                            if (sc.hasNextInt()) {//si el ingreso es un entero, se lo leera
                                contraseña = sc.nextInt();

                                if (p.getContraseña() == contraseña) { //Verificamos que la contraseña ingresada sea la que corresponde al usuario validado
                                    System.out.printf("Bienvenido al sistema %s\n", p.getNombre());
                                    acceso = false;
                                    
                                    //USUARIO TECNICO
                                    if (p.getNivel().equals("tecnico")) {
                                        System.out.print("1.- Información de Clientes \n2.- Información de Proveedores \n");
                                        System.out.print("3.- Información de Países \n4.- Administrar IPs \n");
                                        System.out.println("5.- Información de Tarifa de Región \n6.- Facturar Llamadas");

                                        //Validar que el ingreso sea un número
                                        while (op == 0) {
                                            System.out.print("Elija una opción del menú: ");
                                            if (sc.hasNextInt()) {//si la entrada tiene un entero, se lo leera
                                                op = sc.nextInt();
                                                
                                                while(op < 6){ //Validar que el número ingresado esté entre las opcines del menú
                                                    
                                                    
                                                    if(op == 1){ //INFORMACION DE CLIENTES
                                                        
                                                        
                                                    }
                                                    if(op == 2){//INFORMACION DE PRVEEDORES
                                                        
                                                    }
                                                    if(op == 3){
                                                        Paises cargar = new Paises();
                                                        cargar.InformacionPaises();
                                                    }
                                                    if(op == 4){
                                                        IPs.leerArchivoIPs();
                                                        IPs i = new IPs();
                                                        i.InformacionIPs();
                                                    }
                                                    if(op == 5){
                                                        Tarifas.leerArchivoT();
                                                        Tarifas t =new Tarifas();
                                                        t.MostrarTarifas();
                                                    }
                                                }
                                                
                                            } else {//sino muestra mensaje de error y se limpia el lector
                                                System.out.println("Ingreso incorrecto");
                                                sc.next();
                                            }
                                        }
                                    
                                    //USUARIO ADMIN
                                    }else{  
                                        System.out.print("1.- Detalle Factura Cliente \n");
                                        System.out.print("2.- Reporte llamadas por Clientes por mes\n");
                                        System.out.println("3.- Reporte llamadas por Proveedor por mes");

                                        //Validar el ingreso del número de opción del menú técnico
                                        while (op == 0) {
                                            System.out.print("Elija una opción del menú: ");
                                            if (sc.hasNextInt()) {//si la entrada tiene un entero, se lo leera
                                                op = sc.nextInt();
                                            } else {//sino muestra mensaje de error y se limpia el lector
                                                System.out.println("Ingreso incorrecto");
                                                sc.next();
                                            }
                                        }
                                    }
                                    
                                    
                                }else{
                                    contraseña=0;
                                    break;
                                    
                                }
                            }else{//sino muestra mensaje de error y se limpia el lector
                                System.out.println("Ingreso invalido");
                                sc.next();
                            }
                        }
                            
                    }
                
                }

            } else {//sino se ingresa una cadena de strings para el ususario muestra mensaje de error 
                System.out.println("Ingreso inválido");
                sc.nextLine(); //y se limpia el lector
            }
            
            
        }
        
        

     
    }   
    
}
