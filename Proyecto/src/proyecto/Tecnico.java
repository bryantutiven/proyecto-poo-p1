package proyecto;

import java.util.ArrayList;
import java.util.Scanner;
import static proyecto.Usuario.leerArchivoU;
import static proyecto.Clientes.leerArchivoC;
import static proyecto.Clientes.mostrarInformacion;
import static proyecto.Clientes.validarCliente;
import static proyecto.IPs.leerArchivoIPs;
import static proyecto.Paises.leerArchivoP;
import static proyecto.Proveedores.leerArchivoPr;
import static proyecto.Tarifas.leerArchivoT;
/**
 *
 * @author Us
 */



public class Tecnico extends Usuario {

    ArrayList<Usuario> listaUsuarios = leerArchivoU();
    ArrayList<Clientes> listaClientes = leerArchivoC();
    ArrayList<IPs> listaIPs = leerArchivoIPs();
    ArrayList<Paises> listaPaises = leerArchivoP();
    ArrayList<Proveedores> listaProveedores = leerArchivoPr();
    ArrayList<Tarifas> listaTarifas = leerArchivoT();       
    
    Scanner sc = new Scanner(System.in);
    
    public Tecnico(String id_usuario, int contraseña, String nombre, String nivel) {
        super(id_usuario, contraseña, nombre, nivel);
        this.nivel = nivel; 
    } 
    
    
    public void MenuTecnicoOP1() {
        String consulta = "";
        mostrarInformacion(listaClientes);
        System.out.print("Desea editar información: (S/N)");
        consulta = sc.next();
        while (!consulta.equals("S") || !consulta.equals("N")) {
            System.out.print("Ingrese una opción válida");
            sc.next();
            if (consulta.equals("N")) {
                break;
            } else {
                System.out.print("Ingrese código de cliente: ");
                String codigoCliente = sc.next();
                while (validarCliente(listaClientes, codigoCliente)) {
                    mostrarInformacion(listaClientes, codigoCliente);
                    

                    if (!validarCliente(listaClientes, codigoCliente)) {
                        System.out.print("Ingrese un código válido");
                        sc.next();
                    }
                }

            }

        }

    }
    
}
