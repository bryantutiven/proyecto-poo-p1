/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Proveedores {
    protected int codigo;
    protected String nombre;
    protected String direccion;
    protected int telefono;

    
    public Proveedores() {
        this.codigo = 0;
        this.nombre = "";
        this.direccion = "";
        this.telefono = 0;
    }
    public Proveedores(int codigo, String nombre, String direccion, int telefono) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
    public static ArrayList<Proveedores> leerArchivoPr(){
        ArrayList<Proveedores> listaProveedores = new ArrayList<Proveedores>();


        String csvFile = "proveedores.csv";
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                Proveedores usuario = new Proveedores(Integer.parseInt(datos[0]),datos[1],datos[2],Integer.parseInt(datos[3]));
                listaProveedores.add(usuario);
            }
                      
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return listaProveedores;   
    }
    
}
