
package proyecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class Tarifas implements Comparable<Tarifas> {
    protected int codigoProveedor;
    protected int codigoPais;
    protected String nombreRegion;
    protected int prefijoRegion;
    protected float tarifa;

    public Tarifas() {
        this.codigoProveedor = 0;
        this.codigoPais = 0;
        this.nombreRegion = "";
        this.prefijoRegion = 0;
        this.tarifa = 0;
    }

    public Tarifas(int codigoProveedor, int codigoPais, String nombreRegion, int prefijoRegion, int tarifa) {
        this.codigoProveedor = codigoProveedor;
        this.codigoPais = codigoPais;
        this.nombreRegion = nombreRegion;
        this.prefijoRegion = prefijoRegion;
        this.tarifa = tarifa;
    }

    public int getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(int codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public int getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(int codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getNombreRegion() {
        return nombreRegion;
    }

    public void setNombreRegion(String nombreRegion) {
        this.nombreRegion = nombreRegion;
    }

    public int getPrefijoRegion() {
        return prefijoRegion;
    }

    public void setPrefijoRegion(int prefijoRegion) {
        this.prefijoRegion = prefijoRegion;
    }

    public float getTarifa() {
        return tarifa;
    }

    public void setTarifa(float tarifa) {
        this.tarifa = tarifa;
    }
    
    public static ArrayList<Tarifas> leerArchivoT(){
        ArrayList<Tarifas> listaTarifas = new ArrayList<Tarifas>();


        String csvFile = "tarifas.csv";
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                Tarifas usuario = new Tarifas(Integer.parseInt(datos[0]),Integer.parseInt(datos[1]),datos[2],Integer.parseInt(datos[3]),(int)Float.parseFloat(datos[4]));
                listaTarifas.add(usuario);
            }
                      
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return listaTarifas;   
    }
    

    public void MostrarTarifas(){
        Scanner input = new Scanner(System.in);
        ArrayList<Tarifas> listaTarifas = leerArchivoT();
        String acceso = "true", validacion1="true", validacion2="true";
        int contador =0;
        while(acceso.equals("true")){
            System.out.println("Elija una opcion:"
                    + "\n\t 1. Mostrar informacion ordenadas por codigo de pais."
                    + "\n\t 2. Mostrar informacion ordenada por nombre de region."
                    + "\n\t 3. Editar Tarifa."
                    + "\n\t 4. Salir.");
            String opcion = input.next();
            if(opcion.equals("1")){
                System.out.println("\n Ha elegido la opcion de ordenar codigo por pais.\n");
                System.out.println("Codigo Proveedor \t CodigoPais \t NombreRegion \t Prefijo Region \t Tarifa");
                for(Tarifas t : listaTarifas){
                    System.out.println(t.getCodigoProveedor()+"\t"+t.getCodigoPais()+"\t"+t.getNombreRegion()+"\t"+t.getPrefijoRegion()+"\t"+t.getTarifa()+"\t");               
                }
            }
            else if(opcion.equals("2")){
                System.out.println("\n Ha elegido la opcion de ordenar codigo por nombre Region.\n");
                Collections.sort(listaTarifas);
                System.out.println("Codigo Proveedor \t CodigoPais \t NombreRegion \t Prefijo Region \t Tarifa");
                for(Tarifas t : listaTarifas){
                    System.out.println(t.getCodigoProveedor()+"\t"+t.getCodigoPais()+"\t"+t.getNombreRegion()+"\t"+t.getPrefijoRegion()+"\t"+t.getTarifa());               
                }
            }
            else if(opcion.equals("3")){
                
                while(validacion1.equals("true")){
                            System.out.println("\n Ha elegido la opcion de Editar tarifas.");
                            System.out.println("ingrese prefijo de la region para lozalizar tarifa.");
                            int prefijo = input.nextInt();
                            for(Tarifas t: listaTarifas){
                                contador+=1;
                                if(prefijo == t.getPrefijoRegion()){
                                    System.out.println("IP localizada.");  
                                    while(validacion2.equals("true")){
                                        System.out.println("Agrege nueva tarifa a cambiar.");
                                        String valorTarifa = input.next();
                                        if(valorTarifa.length()>=2){
                                        if(valorTarifa.charAt(0) =='0' && valorTarifa.charAt(1) == '.'){
                                            t.setTarifa((Float.parseFloat(valorTarifa)));
                                            System.out.println("Cambio de codigo Exitoso.");
                                            System.out.println("Codigo Proveedor \t CodigoPais \t NombreRegion \t Prefijo Region \t Tarifa");
                                            for(Tarifas ta: listaTarifas){
                                                System.out.println(ta.getCodigoProveedor()+"\t"+ta.getCodigoPais()+"\t"+ta.getNombreRegion()+"\t"+ta.getPrefijoRegion()+"\t"+ta.getTarifa());
                                            }
                                            validacion1 = "false";
                                            validacion2 = "false";
                                        }
                                        }
                                        else{
                                            System.out.println("Error tarifa. Vuelva  intentarlo");
                                        }
                                    }
                                }
                                else if(contador == listaTarifas.size() && prefijo != t.getPrefijoRegion()){
                                System.out.println("La IP ingresada  no esta en la base de datos. Vuelva a intentarlo.");
                                }
        }
        
    }
            }
            else{
                System.out.println("Ha elegido la opcion salir.");
                acceso = "false";
                
            }
        }
}
    @Override
    public int compareTo(Tarifas t) {
        return nombreRegion.compareTo(t.getNombreRegion());
    }
}
