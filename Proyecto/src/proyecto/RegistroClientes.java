/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Tavo
 */
public class RegistroClientes {
    protected String fecha;
    protected String hora;
    protected String ipFuente;
    protected String ipDestino;
    protected String DNSI;
    protected String ANI;
    protected int duracion;
    protected int codigoCliente;
    protected int codigoProveedor;
    protected int codigoPais;
    protected String costoCliente;
    protected String costoProveedor;
    protected String mes;
    protected String año;
    protected int llamadasCompletadas;
    protected int intentoLlamadas;
    protected float totalPagar;
    
    
    //Constructor "factura de llamadas"
    public RegistroClientes(String fecha, String hora, String ipFuente, String ipDestino, String DNSI, String ANI,
            int duracion, int codigoCliente, int codigoProveedor,int codigoPais, String costoCliente, String costoProveedor){
        this.fecha = fecha;
        this.hora = hora;
        this.ipFuente = ipFuente;
        this.codigoPais = codigoPais;
        this.ipDestino = ipDestino;
        this.DNSI = DNSI;
        this.ANI = ANI;
        this.duracion = duracion;
        this.codigoCliente = codigoCliente;
        this.codigoProveedor = codigoProveedor;
        this.codigoPais = codigoPais;
        this.costoProveedor = costoProveedor;
        this.costoCliente = costoCliente;
    }
    //constructor "Factura por mes y pais"
    public RegistroClientes(String mes,String año, int codigoCliente,int codigoPais, int llamadasCompletadas, int intentoLlamadas,float totalPagar){
        this.mes = mes;
        this.año = año;
        this.codigoCliente = codigoCliente;
        this.codigoPais = codigoPais;
        this.llamadasCompletadas = llamadasCompletadas;
        this.intentoLlamadas = intentoLlamadas;
        this.totalPagar = totalPagar;
    }
    
    public void setMes(String mes) {
        this.mes = mes;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public void setLlamadasCompletadas(int llamadasCompletadas) {
        this.llamadasCompletadas = llamadasCompletadas;
    }

    public void setIntentoLlamadas(int intentoLlamadas) {
        this.intentoLlamadas = intentoLlamadas;
    }

    //getter y setters
    public void setTotalPagar(float totalPagar) {
        this.totalPagar = totalPagar;
    }

    public String getMes() {
        return mes;
    }

    public String getAño() {
        return año;
    }

    public int getLlamadasCompletadas() {
        return llamadasCompletadas;
    }

    public int getIntentoLlamadas() {
        return intentoLlamadas;
    }

    public float getTotalPagar() {
        return totalPagar;
    }
    
    

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setIpFuente(String ipFuente) {
        this.ipFuente = ipFuente;
    }

    public void setIpDestino(String ipDestino) {
        this.ipDestino = ipDestino;
    }

    public void setDNSI(String DNSI) {
        this.DNSI = DNSI;
    }

    public void setANI(String ANI) {
        this.ANI = ANI;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public void setCodigoProveedor(int codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public void setCodigoPais(int codigoPais) {
        this.codigoPais = codigoPais;
    }

    public void setCostoCliente(String costoCliente) {
        this.costoCliente = costoCliente;
    }

    public void setCostoProveedor(String costoProveedor) {
        this.costoProveedor = costoProveedor;
    }

    
    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getIpFuente() {
        return ipFuente;
    }

    public String getIpDestino() {
        return ipDestino;
    }

    public String getDNSI() {
        return DNSI;
    }

    public String getANI() {
        return ANI;
    }

    public int getDuracion() {
        return duracion;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public int getCodigoProveedor() {
        return codigoProveedor;
    }

    public int getCodigoPais() {
        return codigoPais;
    }

    public String getCostoCliente() {
        return costoCliente;
    }

    public String getCostoProveedor() {
        return costoProveedor;
    }
    
    public RegistroClientes(){
        
    }
    
    
    
    @Override
    public String toString() {
        return "RegistroClientes{" + "fecha=" + fecha + ", hora=" + hora + ", "
                + "ipFuente=" + ipFuente + ", ipDestino=" + ipDestino + ", "
                + "DNSI=" + DNSI + ", ANI=" + ANI + ", duracion=" + duracion + ", "
                + "codigoCliente=" + codigoCliente + ", codigoProveedor=" 
                + codigoProveedor + ", codigoPais=" + codigoPais 
                + ", costoCliente=" + costoCliente + ", costoProveedor=" 
                + costoProveedor;
    }
    
    public static ArrayList<RegistroClientes>leerArchivo(){
        ArrayList<RegistroClientes>ListaRegistro = new ArrayList();
        
        
        String csvFile = "llamadas_facturadas.csv";
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                RegistroClientes u = new RegistroClientes(datos[0],datos[1],datos[2],datos[3],datos[4],datos[5],Integer.parseInt(datos[6]),Integer.parseInt(datos[7]),Integer.parseInt(datos[8]),Integer.parseInt(datos[9]),datos[10],datos[11]);
                ListaRegistro.add(u);
            }

              
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
            }
                
        }
    }
        
        return ListaRegistro;
    }
    
    public int[] clientesMes(ArrayList ListaRegistro){
        
        int[] listaMes = new int[11];
        
        for( Object u: ListaRegistro){
            RegistroClientes v = (RegistroClientes)u;
            String dato1 = v.fecha;
            String[] dato2 = dato1.split("/");
            String mes = dato2[1];
            if (mes == "Enero"){
                
              
            
            }if(mes == "Febrero"){
                listaMes[1] += 1;
            
            }if(mes == "Marzo"){
                listaMes[2] += 1;
            }if(mes == "Abril"){
                listaMes[3] += 1;
            }if (mes == "Mayo"){
                listaMes[4] += 1;
            }if (mes == "Junio"){
                listaMes[5] += 1;
            }if (mes == "Julio"){
                listaMes[6] += 1;
            }if (mes == "Agosto"){
                listaMes[7] += 1;    
            }if (mes == "Septiembre"){
                listaMes[8] += 1;
            }if (mes == "Octubre"){
                listaMes[9] += 1;
            }if (mes == "Noviembre"){
                listaMes[10] += 1;
            }if (mes == "Diciembre"){
                listaMes[11] += 1;
            }
       }
        return listaMes;
    
    
    }
    
    
}
