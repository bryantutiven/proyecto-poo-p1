
package proyecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Paises {
    protected String codigo;
    protected String nombre;
    protected int prefijo;

    public Paises() {
        this.codigo = "";
        this.nombre = "";
        this.prefijo = 0;
    }

    public Paises(String codigo, String nombre, int prefijo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.prefijo = prefijo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(int prefijo) {
        this.prefijo = prefijo;
    }
    
    public int ValidarCodigo(String codigo){
        String numeros="0123456789";
        for(int i=0; i<codigo.length(); i++){
            if (numeros.indexOf(codigo.charAt(i),0)!=-1){
                int v=1;
            return v;
            }
        }
        int f=0;
        return f;
    }
    
    public static ArrayList<Paises> leerArchivoP(){
        ArrayList<Paises> listaPaises = new ArrayList<Paises>();


        String csvFile = "paises.csv";
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                Paises usuario = new Paises(datos[0],datos[1],Integer.parseInt(datos[2]));
                listaPaises.add(usuario);
            }
                      
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return listaPaises;  
    
    }
    
    public void InformacionPaises(){//Muestra informacion del los paises.
        ArrayList<Paises> listaPaises = leerArchivoP();
        Scanner input = new Scanner(System.in);
        String opcion1="true",op2="true", validacion1 = "true", validacion2="true",validacion3 = "true", validacion4="true";
        while(opcion1.equals("true")){
        System.out.println("Elija una opcion entre:\n"
                + "\t1. Mostrar Listado de paises.\n"
                + "\t2. Editar informacion de algun pais.\n"
                + "\t3. Volver al menu principal.\n");
        String opcion = input.next();
        if(opcion.equals("1")){
            System.out.println("Ha elegido la opcion 1.");
            System.out.println("Codigo \t Pais \t Prefijo");
            for(Paises p: listaPaises){
                System.out.println(p.getCodigo()+"\t"+p.getNombre()+"\t"+p.getPrefijo());
            }
        }
        else if(opcion.equals("2")){
            System.out.println("Ha elegido la opcion 2.");
            while(validacion1.equals("true")){
                System.out.println("Ingrese codigo del pais.");
                String cod = input.next();
                for(Paises p : listaPaises){   
                  if(cod.equalsIgnoreCase(p.getCodigo())){
                      System.out.println("Ingreso de codigo valido.");
                      System.out.println(p.getCodigo()+"\t"+p.getNombre()+"\t"+p.getPrefijo());
                      while(op2.equals("true")){
                      System.out.print("Ingrese opcion a editar:\n"
                              + "\t1. Codigo.\n"
                              + "\t2. Pais.\n"
                              + "\t3. Prefijo.\n "
                              + "\t4. Salir al menu informacion de paises.\n");
                      String opcion2 =input.next();
                      if(opcion2.equals("1")){
                          System.out.println("Ha elegido la opion 1:");
                          while(validacion2.equals("true")){
                          System.out.println("Ingrese  nuevo codigo:");
                          String codigo = input.next();
                          codigo = codigo.toUpperCase();
                          Paises pai = new Paises();
                          int r = pai.ValidarCodigo(codigo);
                          if(r == 0){
                                p.setCodigo(codigo);
                                System.out.println("Codigo \t Pais \t Prefijo");
                                for(Paises pa: listaPaises){
                                    System.out.println(p.getCodigo()+"\t"+p.getNombre()+"\t"+p.getPrefijo());
                                }
                                validacion2="false";
                          }
                          else{
                              System.out.println("Error de tipo de dato, ingrese opcion que sea tipo String");
                          }
                        }
                      }
                      else if(opcion2.equals("2")){
                          System.out.println("Ha elegido la opion 2:");
                          while(validacion3.equals("true")){
                            System.out.println("Ingrese  pais:");
                            String pais = input.next();
                            Paises pai = new Paises();
                            int r = pai.ValidarCodigo(pais);
                            if(r==0){
                                p.setNombre(pais);
                                System.out.println("Codigo \t Pais \t Prefijo");
                                for(Paises pa: listaPaises){
                                    System.out.println(p.getCodigo()+"\t"+p.getNombre()+"\t"+p.getPrefijo());
                                    validacion3="false";
                                }
                            }
                          else{
                              System.out.println("Error de tipo de dato, ingrese opcion que sea tipo String");
                            }
                        }
                        }
                      else if(opcion2.equals("3")){
                          System.out.println("Ha elegido la opion 3:");
                          while(validacion4.equals("true")){
                            System.out.println("Ingrese  nuevo prefijo:");
                            String prefijo = input.next();
                            if (prefijo.matches("[0-9]*")==true){
                            p.setPrefijo(Integer.parseInt(prefijo));
                            System.out.println("Codigo \t Pais \t Prefijo");
                            for(Paises pa: listaPaises){   
                                System.out.println(p.getCodigo()+"\t"+p.getNombre()+"\t"+p.getPrefijo());
                                validacion4="false";
                            }
                          }
                          else{
                              System.out.println("Error de prefijo. Digite un prefijo que contenga solo numeros.");
                            }
                          }
                      }
                      else if(opcion2.equals("4")){
                          System.out.println("Ha elegido la opion 4 Volver al menu:");
                          validacion1="false";
                          op2="false";
                      }
                 }
                
            }
        }
            }
        }
        else if(opcion.equals("3")){
            System.out.println("Ha elegido la opcion salir.");
            opcion1="false";
        }
        else{
            System.out.println("Error de opcion  vuelva a intentarlo.");
        }
    }
}
}

